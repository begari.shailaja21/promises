//async will always return a Promise

// async function name(){
//     return "hello"
// }
// // console.log(name())
// //async will return the promise so we can wrirte here .the
// name().then((x)=>console.log(x))

//

//the async function without using awaoit also it will work but await it will not work its showing error.

//The await keyword can only be used inside an async function.//

// async function name(){
//     return "shailu"
// }
// name().then(
//  function(val){
//      console.log(val)
//  } ,
//  function(error){
//     console.log(error)
//  }  
// )

function cal(a,b){
    return new Promise(function (resolve,reject) {
        setTimeout(() => {
            resolve(a+b)
        }, 2000);
    })
}
const add=async function (){
    return await cal(3,5)
}
add().then(x=>console.log(x))